# Rivet
To run locally:
1. setupATLAS
2. source setup.sh
3. To compile >rivet-buildplugin RivetATLAS_2019_I1720438.so RivetATLAS_2019_I1720438.cc
4. Modify the file to the desired input file in the Run.py
5. athena Run.py
6. create plots by 
>rivet-mkhtml --mc-errs  myanalysis_output.yoda


To run on the grid, follow instructions here:
https://gitlab.cern.ch/atlas-physics/pmg/tools/systematics-tools#i-am-using-rivet/yoda-for-truth-level-studies-or-to-prepare-a-rivet-routine-for-my-analysis
